import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    newList: '',
  };

  handleTodos = (event) => {
    if (event.key === "Enter") {
      const newList = {
        "userId": 1,
        "id": Math.random(),
        "title": event.target.value,
        "completed": false
      };

      this.setState(state => (({
        todos: [...this.state.todos, newList],
      })));

      event.target.value = ""
    };
    console.log(this.state)
    console.log("Anything")
  };

  /// 2 more functions needed

  handleToggleCheck = (todo) => {
    const todoCopy = [...this.state.todos]
    const newVar = todoCopy.map(todoNewList => {
      if (todo === todoNewList.id) {
        todoNewList.completed = !todoNewList.completed
      }
      return todoNewList
    })
    this.setState( { todos: newVar })
  }

  handleTodoDelete = todoId => event => {
    console.log('it works')
    const removeTodos = this.state.todos.filter(
      todo => todo.id !== todoId
    )
    this.setState({ todos: removeTodos})
  }

  handleClearCompleted = todoId => event => {
   console.log('it works')
    const removeAllTodos = this.state.todos.filter(
      todo => !todo.completed 
    )
    this.setState({ todos: removeAllTodos})
  }

  render() {
    return (
      <section className="todoapp" >
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" onKeyDown={this.handleTodos} autoFocus />
        </header>
        <TodoList todos={this.state.todos} handleTodoDelete={this.handleTodoDelete}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleClearCompleted(this.state.todos.id)}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={this.props.completed.handleToggleCheck}/>
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleTodoDelete(this.props.id)}/>
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem title={todo.title} 
            completed={todo.completed} 
            handleTodoDelete={this.props.handleTodoDelete} 
            key={todo.id}
            id={todo.id}
            
            
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;